from django_elasticsearch_dsl import Document
from django_elasticsearch_dsl.registries import registry

from .models import User

@registry.register_document
class UserDocument(Document):
    class Index:
        # Name of the Elasticsearch index
        name = 'users'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 2,
                    'number_of_replicas': 3}

    class Django:
        model = User # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'first_name',
            'last_name',
            'username',
            'date_joined',
            'email',
            'is_superuser',
            'is_active',
            'is_staff',
            'admin',
        ]