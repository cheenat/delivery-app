from django.shortcuts import render, redirect


def auth_required(view_func):
    def wrap(request, *args, **kwargs):
        if request.user.is_authenticated:
            return view_func(request, *args, **kwargs)
        else:
            return redirect("/login_form/")
    return wrap


def staff_required(view_func):
    def wrap(request, *args, **kwargs):
        if request.user.is_staff:
            return view_func(request, *args, **kwargs)
        else:
            return render(request, 'logregform.html', {'error_message': 'you have no permission to perform this action, login to staff'})
    return wrap