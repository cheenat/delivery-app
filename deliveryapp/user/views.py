from .documents import UserDocument
from django.http import JsonResponse
from django.shortcuts import render

import json


def search_users(request):
    data = json.loads(request.body)
    search_result = UserDocument.search().query("match", username=data['search_input'])
    search_result_list = [user.username for user in search_result]

    return JsonResponse(search_result_list, safe=False, json_dumps_params={'ensure_ascii':False})


def search_form(request):
    return render(request, 'elasticsearch-users.html')