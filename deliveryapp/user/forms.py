from django import forms
from .models import User

class UserLogRegForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'password']