from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry

from .models import Order

@registry.register_document
class OrderDocument(Document):
    region = fields.ObjectField(properties={
        'number': fields.IntegerField(),
    })
    delivery_days = fields.ObjectField(properties={
        'day': fields.TextField(),
        'comment': fields.TextField(),
    })
    delivery_hours = fields.ObjectField(properties={
        'time_interval': fields.TextField(),
        'comment': fields.TextField(),
    })
    # assigned_courier = fields.ObjectField(properties={
    #     'time_interval': fields.TextField(),
    #     'comment': fields.TextField(),
    # })

    class Index:
        # Name of the Elasticsearch index
        name = 'orders'
        # See Elasticsearch Indices API reference for available settings
        settings = {'number_of_shards': 2,
                    'number_of_replicas': 3}

    class Django:
        model = Order # The model associated with this Document

        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'weight', 
            'complete_time',
            # 'assigned_courier'
        ]