# Generated by Django 3.1.7 on 2021-04-20 18:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0015_auto_20210419_1604'),
    ]

    operations = [
        migrations.AddField(
            model_name='deliveryday',
            name='comment',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
