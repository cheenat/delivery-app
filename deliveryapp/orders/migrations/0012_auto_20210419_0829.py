# Generated by Django 3.1.7 on 2021-04-19 08:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0011_order_region'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='region',
            options={'verbose_name': 'Регион', 'verbose_name_plural': 'Регионы'},
        ),
        migrations.RemoveField(
            model_name='order',
            name='delivery_hours',
        ),
    ]
