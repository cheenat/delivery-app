from django.utils import timezone
from django.contrib.postgres.fields import ArrayField
from django.db import models
from deliveryapp import settings


class Region(models.Model):
    number = models.PositiveIntegerField(blank=True, null=True, verbose_name='Номер региона')

    def __str__(self):
        return 'Регион №' + str(self.number)

    class Meta:
        ordering = ['number']
        verbose_name = 'Регион'
        verbose_name_plural = 'Регионы'


class DeliveryDay(models.Model):
    day = models.CharField(
        max_length=20, 
        blank=True, 
        null=True, 
        verbose_name='День недели'
    )
    comment = models.CharField(
        max_length=100, 
        blank=True, 
        null=True, 
        verbose_name='Комментарии к дню недели'
    )

    def __str__(self):
        return self.day

    class Meta:
        # ordering = ['day']
        verbose_name = 'День доставки'
        verbose_name_plural = 'Дни доставки'


class Order(models.Model):
    customer = models.ForeignKey(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, blank=True, null=True, verbose_name='Получатель')
    weight = models.FloatField(blank=True, null=True, verbose_name='Вес посылки')
    region = models.ForeignKey(
        Region, 
        on_delete=models.SET_NULL, 
        null=True, 
        blank=True, 
        verbose_name='Регион доставки заказа'
    )
    delivery_days = models.ManyToManyField(
        DeliveryDay, 
        verbose_name='Дни доставки посылки'
    )
    delivery_hours = models.ManyToManyField(
        to='couriers.WorkingHours', 
        verbose_name='Часы доставки'
    )
    assigned_courier = models.ForeignKey(
        to='couriers.Courier', 
        on_delete=models.SET_NULL, 
        null=True, 
        blank=True, 
        verbose_name='ID назначенного курьера'
    )
    complete_time = models.DateTimeField(
        default=timezone.now, 
        # blank=True, 
        # null=True, 
        verbose_name='Время доставки посылки'
    )

    def __str__(self):
        return 'Заказ №' + str(self.id)

    class Meta:
        ordering = ['complete_time']
        verbose_name = 'Заказ'
        verbose_name_plural = 'Заказы'