from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, JsonResponse, HttpResponseNotAllowed
from django.shortcuts import get_object_or_404
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required

from orders.models import Order, Region, DeliveryDay
from couriers.models import WorkingHours
from .serializers import OrderSerializer, CompleteOrderSerializer

from rest_framework.parsers import JSONParser

from user.decorators import auth_required, staff_required


# @login_required(login_url='/login_form') redirect_url
@auth_required
@staff_required
def get_orders(request):
	orders = Order.objects.filter(user=request.user)
	return JsonResponse({'orders': [{'id': order.id, 'date': order.complete_time} for order in orders]})


@csrf_exempt
@staff_required
def orders_handler(request):  # add user who created order
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = OrderSerializer(data=data, many=True)
        if serializer.is_valid():
            added_orders = serializer.save()
            courier_ids = [{"id": x.id} for x in added_orders]
            return JsonResponse({"orders": courier_ids}, status=201)
        return JsonResponse(serializer.errors, status=400, safe=False)

    return HttpResponseNotAllowed(['POST'])


@csrf_exempt
@auth_required
def complete_order(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CompleteOrderSerializer(data=data)
        if serializer.is_valid():
            order = get_object_or_404(Order, id=data.get('order_id'))
            if order.assigned_courier_id != data.get('courier_id'):
                return JsonResponse(
                    {'assigned_courier_error': "order not assigned to courier"
                        f" with id={data.get('courier_id')}"}, 
                    status=400
                )
            
            order.complete_time = data.get('complete_time')
            order.save()
            return JsonResponse({"order_id": data.get('order_id')}, status=200)
        return JsonResponse({'validation_error': serializer.errors}, status=400)

    return HttpResponseNotAllowed(['POST'])
