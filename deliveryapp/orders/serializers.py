from .models import Order

from rest_framework import serializers


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ['weight', 'region', 'delivery_days', 'delivery_hours', 'assigned_courier', 'complete_time']

class CompleteOrderSerializer(serializers.Serializer):
    courier_id = serializers.IntegerField(min_value=1)
    order_id = serializers.IntegerField(min_value=1)
    complete_time = serializers.DateTimeField()

    # class Meta:
    #     fields = ['courier_id', 'order_id', 'complete_time']