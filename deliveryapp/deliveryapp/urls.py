from django.contrib import admin
from django.urls import path, include

from couriers import views as couriers_views
from orders import views as orders_views
from user import views as user_views
from . import views

from .viewssets import UserModelViewSet, CourierModelViewSet, OrderModelViewSet
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'users', UserModelViewSet, basename='user')
router.register(r'couriers/viewset', CourierModelViewSet, basename='courier')
router.register(r'orders/viewset', OrderModelViewSet, basename='order')


urlpatterns = [
    path('admin/', admin.site.urls),
    path('social_auth/', include('social_django.urls', namespace='social')),

    path('login_form/', views.login_form, name='Login form page'),
    path('login/', views.login_view, name='log a user in'),
    path('logout/', views.logout_view, name='log a user out'),
    path('add_courier_form/', views.add_courier_form, name='Add courier html form'),
    path('add_one_courier/', views.add_one_courier, name='Add only one courier from form'),

    path('couriers/', couriers_views.couriers, name="Return courier list"),  # curl localhost:8000/couriers/
    path('couriers/new/', couriers_views.add_couriers, name="Adds new courier(s)"),  # curl -d '[{"firstname": "Alex", "lastname": "Kuncev", "courier_type": "bike", "regions": [2, 3], "working_hours": [1, 2]}, {"firstname": "Alex2", "lastname": "Kuncev", "courier_type": "bike", "regions": [2, 3], "working_hours": [1, 2]}]' localhost:8000/couriers/new/
    path('couriers/<int:courier_id>/', couriers_views.courier_info, name="Courier info by ID"),  # curl localhost:8000/couriers/1/
    path('couriers/<int:courier_id>/edit/', couriers_views.courier_edit, name="Edit courier info"),  # curl -X PUT -d '{"regions": [1]}' localhost:8000/couriers/1/edit/
    path('couriers/<int:courier_id>/delete/', couriers_views.courier_delete, name="Delete courier"),  # curl -X DELETE localhost:8000/couriers/100/delete/

    path('orders/', orders_views.get_orders, name="Return order list for current user"),  # curl localhost:8000/orders/
    path('orders/assign/', couriers_views.assign_orders_to_courier, name="Assigns the maximum number of free orders that match the parameters by courier ID"),  # curl -X POST -d '{"courier_id": 2}' 127.0.0.1:8000/orders/assign/
    path('orders/new/', orders_views.orders_handler, name="Adds new order(s)"),  # curl -d '[{"weight": 0.23, "region": 2, "delivery_days": [1,2,3,4,5], "delivery_hours": [1, 3]}]' localhost:8000/orders/new/
    path('orders/complete/', orders_views.complete_order, name="Marks the order as completed by order ID"),  # curl -X POST -d '{"courier_id": 2, "order_id": 3, "complete_time": "2021-01-10T10:33:01.42Z"}' localhost:8000/orders/complete/

    path('test/search/', views.search_test, name="Testing elasticsearch"),
    path('test/search_form/', views.search_test_form, name="Testing elasticsearch form"),

    path('user/search/', user_views.search_users, name="ElasticSearch users"),  #
    path('user/search_form/', user_views.search_form, name="ElasticSearch users form"),  #

    path('cent_client/', views.centrifugo_client, name="Centrifugo client"),
    path('cent_send/', views.centrifugo, name="Api to send into centrifugo channel"),

    path('homepage/', views.homepage_html, name="home"),  # curl localhost:8000/moscow/ # /
    path('json/', views.json_resp, name="json response"),
]

urlpatterns += router.urls