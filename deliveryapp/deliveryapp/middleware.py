from django.http import HttpResponseRedirect


class LoginMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response
    def __call__(self, request):
        return self.get_response(request)
    def process_view(self, request, view_func, view_args, view_kwargs):
        allowed_urls=['/homepage/', '/login_form/', '/login/', '/logout/']

        if request.path in allowed_urls:
            return None
        if request.user.is_authenticated:
            return None

        return HttpResponseRedirect('/login_form/')