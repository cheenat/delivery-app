from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from django.http import JsonResponse, HttpResponse, HttpResponseNotAllowed
from django.contrib.auth import authenticate, login, logout
from django.contrib.sessions.models import Session

from couriers.forms import CourierForm
from user.forms import UserLogRegForm
from user.models import User

from django.contrib.auth.decorators import login_required
from user.decorators import auth_required, staff_required

from elasticsearch import Elasticsearch

import json

from cent import Client, CentException


@auth_required
def add_courier_form(request):
    return render(request, 'add_one_courier.html')


@staff_required
def add_one_courier(request):
    if request.method == 'POST':
        form = CourierForm(request.POST)
        if form.is_valid():
            courier = form.save()
            return JsonResponse({"courier_id": courier.id}, status=201)
        
        html = f'<b>{form.errors}</b>redirecting back...\
            <script>setTimeout(() => window.location.replace("/add_courier_form/"), 2000);</script>'
        return HttpResponse(html)

    return HttpResponseNotAllowed(['POST'])


def login_form(request):
    return render(request, 'logregform.html')


def login_view(request, redirect_url=''):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        request.session['username'] = username
        return redirect('/homepage/')
    else:
        return render(request, 'logregform.html', {'error_message': 'invalid login or password'})
        return JsonResponse({'user_error': "invalid login or password"}, safe=False)


def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return redirect('/homepage/')

    return HttpResponse(status=204)

def homepage_html(request):
    if request.COOKIES.get('sessionid'):
        session = Session.objects.get(session_key=request.COOKIES.get('sessionid'))
        if session is not None:
            session_data = session.get_decoded()
            uid = session_data.get('_auth_user_id')
            user = User.objects.get(id=uid)

    return render(request, 'index.html')


def json_resp(request):
    return JsonResponse({"json": "resp"})


def search_test(request):
    print('search_test')
    es = Elasticsearch()
    data = json.loads(request.body)
    # if not data['search_input']:
    #     return JsonResponse({[]})
    print(data['search_input'], 'asldkflklf')
    # search_result = es.search().query("match", quote=data['search_input'])
    query_value = f"*{data['search_input']}*"
    search_result = es.search(index="test-index-msu", body={"query": {"wildcard": {"quote": query_value}}})  # case_insensitive=True
    # search_result = es.search(index="test-index-msu", body={"query": {"query_string": {"query": query_value, "fields" : ["quote"]}}})
    # search_result = UserDocument.search().query("match", username=data['search_input'])
    print(search_result['hits']['hits'])
    # return JsonResponse({})
    search_result_list = [res['_source']['quote'] for res in search_result['hits']['hits']]
    print(search_result_list)

    return JsonResponse(search_result_list, safe=False, json_dumps_params={'ensure_ascii':False})


def search_test_form(request):
    return render(request, 'test-es.html')


def centrifugo_client(request):
    return render(request, 'cent_client.html')


def centrifugo(request):
    client = Client("http://localhost:8000", api_key="3b0ed747-c4c3-4f1a-8318-2141c495a898", timeout=1)

    params = {
        "channel": "channel",
        "data": "hello world"
    }

    client.add("publish", params)

    try:
        result = client.send()
    except CentException:
        return JsonResponse({"error": CentException})
        # handle exception
    else:
        print(result)

    return JsonResponse({"send": "success"})