from user.models import User
from orders.models import Order
from couriers.models import Courier
from django.shortcuts import get_object_or_404
from user.serializers import UserSerializer
from couriers.serializers import CourierSerializer
from orders.serializers import OrderSerializer
from rest_framework import viewsets
from rest_framework.response import Response

class UserViewSet(viewsets.ViewSet):
    def list(self, request):
        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = User.objects.all()
        user = get_object_or_404(queryset, pk=pk)
        serializer = UserSerializer(user)
        return Response(serializer.data)

class UserModelViewSet(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

class CourierModelViewSet(viewsets.ModelViewSet):
    serializer_class = CourierSerializer
    queryset = Courier.objects.all()

class OrderModelViewSet(viewsets.ModelViewSet):
    serializer_class = OrderSerializer
    queryset = Order.objects.all()