# Generated by Django 3.1.7 on 2021-04-21 11:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('couriers', '0012_remove_courier_working_hours'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='courier',
            name='wh_mtm',
        ),
    ]
