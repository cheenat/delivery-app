# Generated by Django 3.1.7 on 2021-04-22 16:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0019_auto_20210421_1159'),
        ('couriers', '0015_auto_20210421_1114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='courier',
            name='courier_type',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Тип курьера, пеший и т д'),
        ),
        migrations.AlterField(
            model_name='courier',
            name='first_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Имя курьера'),
        ),
        migrations.AlterField(
            model_name='courier',
            name='last_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Фамилия курьера'),
        ),
        migrations.AlterField(
            model_name='courier',
            name='region',
            field=models.ManyToManyField(to='orders.Region', verbose_name='Регион в котором курьер доставляет посылки'),
        ),
        migrations.AlterField(
            model_name='workinghours',
            name='comment',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Комментарий'),
        ),
        migrations.AlterField(
            model_name='workinghours',
            name='time_interval',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Интервал рабочих часов'),
        ),
    ]
