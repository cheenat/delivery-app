from couriers.models import Courier

from rest_framework import serializers

class CourierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Courier
        fields = ['firstname', 'lastname', 'courier_type', 'regions', 'working_hours']