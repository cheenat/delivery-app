from django.contrib.auth.models import AbstractUser
from django.db import models
from deliveryapp import settings


class WorkingHours(models.Model):
    time_interval = models.CharField(max_length=30, blank=True, null=True, verbose_name='Интервал рабочих часов')
    comment = models.CharField(max_length=100, blank=True, null=True, verbose_name='Комментарий')

    def __str__(self):
        return self.time_interval

    class Meta:
        ordering = ['time_interval']
        verbose_name = 'Рабочие часы'
        verbose_name_plural = 'Рабочие часы'


class Courier(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE, blank=True, null=True)
    firstname = models.CharField(max_length=30, blank=True, null=True, verbose_name='Имя курьера')
    lastname = models.CharField(max_length=30, blank=True, null=True, verbose_name='Фамилия курьера')
    courier_type = models.CharField(max_length=30, blank=True, null=True, verbose_name='Тип курьера, пеший и т д')
    regions = models.ManyToManyField(to='orders.Region', verbose_name='Регион в котором курьер доставляет посылки')
    working_hours = models.ManyToManyField(WorkingHours, verbose_name='Часы доставки')
    
    def __str__(self):
        return self.firstname + ' ' + self.lastname

    class Meta:
        ordering = ['lastname']
        verbose_name = 'Курьер'
        verbose_name_plural = 'Курьеры'