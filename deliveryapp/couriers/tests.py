from django.contrib.auth.models import AnonymousUser
from user.models import User
from django.test import RequestFactory, TestCase

from .views import couriers, add_couriers, courier_info, courier_edit, courier_delete
import sys, os

# sys.stdout = open(os.devnull, 'w')  # uncomment


class CourierTest(TestCase):
    fixtures = ['/Users/cheena/Documents/Backend-11/hw-4,5/deliveryapp/couriers/fixtures/dump.json',]

    def test_return_all_courier(self):
        # Create an instance of a GET request.
        factory = RequestFactory()
        request = factory.get('/couriers/')

        request.user = User.objects.get(pk=1)
        print(User.objects.filter(), 'all users')
        response = couriers(request)
        self.assertEqual(response.status_code, 200)
        # print(response.content, 'sdresponse.content')
        self.assertEqual(response.content, b'[{"firstname": "Alex", "lastname": "Kuncev", '
            b'"courier_type": "foot", "regions": [2, 3], "working_hours": [1, 2]}]')

    def test_add_couriers(self):
        factory = RequestFactory()
        data = [{"firstname": "Alex", "lastname": "Kuncev", "courier_type": "bike", "regions": [2, 3], "working_hours": [1, 2]}]
        request = factory.post('/couriers/new/', data, content_type='application/json')

        response = add_couriers(request)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.content, b'{"couriers": [{"id": 2}]}')

    def test_get_courier_info(self):
        factory = RequestFactory()
        request = factory.get('/couriers/1/')

        response = courier_info(request, 1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"firstname": "Alex", "lastname": "Kuncev", '
            b'"courier_type": "foot", "regions": [2, 3], "working_hours": [1, 2]}')

    def test_edit_courier_info(self):
        factory = RequestFactory()
        data = {"regions": [1]}
        request = factory.put('/couriers/1/edit/', data, content_type='application/json')

        response = courier_edit(request, 1)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b'{"firstname": "Alex", "lastname": "Kuncev", '
            b'"courier_type": "foot", "regions": [1], "working_hours": [1, 2]}')

    def test_delete_courier(self):
        factory = RequestFactory()
        request = factory.delete('/couriers/1/delete/')

        response = courier_delete(request, 1)
        self.assertEqual(response.status_code, 204)