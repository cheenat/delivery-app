import json
import operator
from functools import reduce
from datetime import datetime

from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.db.models import Q

from .tasks import send_email_to_admin

from couriers.models import Courier, WorkingHours
from couriers.serializers import CourierSerializer
from orders.models import Order, Region

from rest_framework.parsers import JSONParser

from user.decorators import auth_required, staff_required


@csrf_exempt
# @staff_required
def add_couriers(request):
    if request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = CourierSerializer(data=data, many=True)
        if serializer.is_valid():
            added_couriers = serializer.save()
            courier_ids = [{"id": x.id} for x in added_couriers]
            for courier in added_couriers:
                print('cosdjfsldkf', courier)
                send_email_to_admin(courier)
            return JsonResponse({"couriers": courier_ids}, status=201)
        return JsonResponse(serializer.errors, status=400, safe=False)

    return HttpResponseNotAllowed(['POST'])


@staff_required
def couriers(request):
    if request.method == 'GET':
        couriers = Courier.objects.all()
        serializer = CourierSerializer(couriers, many=True)
        return JsonResponse(serializer.data, safe=False)

    return HttpResponseNotAllowed(['GET'])


@staff_required
def courier_edit(request, courier_id):
    courier = get_object_or_404(Courier, id=courier_id)
    
    if request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = CourierSerializer(courier, data=data, partial=True)
        if serializer.is_valid():
            serializer.save()  # delete
            return JsonResponse(serializer.data)
        return JsonResponse(serializer.errors, status=400)
    
    return HttpResponseNotAllowed(['PUT'])  # decorator api_view drf 


@staff_required
def courier_delete(request, courier_id):
    courier = get_object_or_404(Courier, id=courier_id)

    if request.method == 'DELETE':
        courier.delete()
        return JsonResponse({}, status=204)

    return HttpResponseNotAllowed(['DELETE'])


# @staff_required
def courier_info(request, courier_id):
    if request.method == 'GET':
        courier = get_object_or_404(Courier, id=courier_id)
        serializer = CourierSerializer(courier)
        return JsonResponse(serializer.data, safe=False)
    
    return HttpResponseNotAllowed(['GET'])


@csrf_exempt
@auth_required
def assign_orders_to_courier(request):
    cap = {'foot': [0, 10], 'bike': [0, 15], 'car': [0, 50]}
    if request.method == 'POST':
        data = json.loads(request.body)
        if not data or 'courier_id' not in data:
            return JsonResponse({'error': 'no content or courier_id'}, status=404)

        courier = get_object_or_404(Courier, id=data['courier_id'])
        courier_regions = list(courier.region.all().values_list('id', flat=True))
        courier_wh = list(courier.wh_mtm.all().values_list('id', flat=True))
        orders = Order.objects.filter(
            Q(
                region__in=courier_regions,
                delivery_hours__in=courier_wh,
                weight__range=cap[courier.courier_type],
                complete_time__isnull=True,
                assigned_courier__isnull=True,
            ) | 
            Q(assigned_courier=courier),
        )

        if orders.exists():
            order_ids = [{"id": x.id} for x in orders]
            assign_time = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-4]+'Z'
            return JsonResponse({"orders": orders_ids, "assign_time": assign_time}, status=200)
        return JsonResponse({"orders": []}, status=200)

        return HttpResponseNotAllowed(['POST'])
