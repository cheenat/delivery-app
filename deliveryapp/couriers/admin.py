from django.contrib import admin
from couriers.models import Courier, WorkingHours
from orders.models import Order, Region, DeliveryDay

from django.contrib.auth.admin import UserAdmin
from user.models import User


# Register your models here.
admin.site.register(Order)
admin.site.register(Region)
admin.site.register(DeliveryDay)
admin.site.register(Courier)
admin.site.register(WorkingHours)
admin.site.register(User, UserAdmin)
UserAdmin.add_fieldsets = (
    (None, {
        'classes': ('wide',),
        'fields': ('username', 'email', 'password1', 'password2',)
    }),
)
# UserAdmin.list_display += ('email',)

# stripe_admin_site = StripeAdminSite(name='Stripe')
# from django.contrib.auth.admin import UserAdmin
# stripe_admin_site.register(User, UserAdmin)

# admin.site.register(User, UserAdmin)

import pprint
from django.contrib.sessions.models import Session
class SessionAdmin(admin.ModelAdmin):
    def _session_data(self, obj):
        return pprint.pformat(obj.get_decoded()).replace('\n', '<br>\n')
    _session_data.allow_tags=True
    list_display = ['session_key', '_session_data', 'expire_date']
    readonly_fields = ['_session_data']
    exclude = ['session_data']
    date_hierarchy='expire_date'
admin.site.register(Session, SessionAdmin)