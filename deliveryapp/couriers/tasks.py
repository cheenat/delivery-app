# Create your tasks here

from celery import shared_task

from time import ctime

from django.core.mail import send_mail
from django.conf import settings

from orders.models import Order



@shared_task
def add(x, y):
    return x + y


@shared_task
# @celery.task(time_limit=20)
def mul(x, y):
    return x * y


@shared_task
def send_email_to_admin(courier):
    send_mail(
        'New courier created',
        f"Created new courier {courier.firstname} {courier.lastname} with id={courier.id}",
        'from@yourdjangoapp.com',
        settings.ADMINS,
        fail_silently=False,
    )


@shared_task
def return_amount_of_orders_rn():
    print('retunr amoiuns dkofmksdfk')
    amount_of_orders = len(Order.objects.filter())
    f = open("couriers/beat_logs.txt", "a")
    f.write('\n')
    f.write(f'Orders at {ctime()}: {amount_of_orders}')
    f.close()

    return amount_of_orders