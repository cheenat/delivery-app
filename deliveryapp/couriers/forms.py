from django import forms
from .models import Courier

class CourierForm(forms.ModelForm):
    class Meta:
        model = Courier
        fields = ['firstname', 'lastname', 'courier_type', 'regions', 'working_hours']